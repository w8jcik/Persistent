#pragma once

#include <string.h>

class Persistent {
  const char* name;
  int store_id;
  int value;
  int observed_value;
  int default_value;
  int min_value;
  int max_value;
  int changed_time;

  void (*observer)(int);
  void (*serializer)(Persistent&, int);
  int (*deserializer)(Persistent&);
  void (*logger)(const char*);

public:
  Persistent(void);
  int set(int);
  int get(void);
  void set_logger(void (*new_logger)(const char*));
  void set_name(const char*);
  const char* get_name(void);
  int load(void);
  void set_default_value(int new_default_value);
  void load_default_value();

  void set_observer(void (*new_observer)(int));
  void set_serializer(void (*new_serializer)(Persistent&, int));
  void set_deserializer(int (*new_deserializer)(Persistent&));
  void set_store_id(int);
  int get_store_id(void);

  void set_min_value(int);
  void set_max_value(int);
  int get_min_value(void);
  int get_max_value(void);

  int get_changed_time(void);
  void set_changed_time(int new_time);
};
