shared_flags = -Wall -pedantic -fdiagnostics-color=always

help:
	@echo ""
	@echo "  make tests  -- compile and execute tests for the Persistent class"
	@echo ""

clean:
	rm -f test.o

tests:
	g++ -std=c++98 $(shared_flags) Persistent.cxx test.cxx -o test.o && ./test.o
	g++ -std=c++11 $(shared_flags) Persistent.cxx test.cxx -o test.o && ./test.o
	g++ -std=c++14 $(shared_flags) Persistent.cxx test.cxx -o test.o && ./test.o
	g++ -std=c++17 $(shared_flags) Persistent.cxx test.cxx -o test.o && ./test.o
	@make -s clean

all:
	@make -s tests
