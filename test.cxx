#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "Persistent.h"

int message_count = 0;
int serialization_count = 0;
int deserialization_count = 0;

Persistent feed_time, fan_speed, valve_overriden;

int number_addressed_memory[] = {0, 0, 5};

void store(Persistent & self, int new_value) {
  serialization_count += 1;
  
  //~ printf("saving as [%s] file\n", self.get_name());
  //~ printf("saving also at memory address [%d]\n", self.get_store_id());
  
  number_addressed_memory[self.get_store_id()] = new_value;
}

int retrieve(Persistent & self) {
  deserialization_count += 1;
  
  //~ printf("retrieving data from [%s] file\n", self.get_name());
  //~ printf("retrieving data from [%d] memory address \n", self.get_store_id());
  
  return number_addressed_memory[self.get_store_id()];
}

void logger(const char* message) {
  message_count += 1;
  
  //~ printf("%s\n", message);
}

int main() {
  
  /*
   * test the logger behaviour and an immunity to the logger absence
   * test if lack of name is permitted
   */ 
  
  assert(strcmp(feed_time.get_name(), "unnamed") == 0);
  
  feed_time.set_logger(logger);
  
  assert(strcmp(feed_time.get_name(), "unnamed") == 0);
  
  assert(message_count == 1);
  
  /*
   * test if a name can be assigned successfully
   */
  
  feed_time.set_name("feed_time");
  assert(strcmp(feed_time.get_name(), "feed_time") == 0);
  
  /*
   * test if a store_id can be assigned successfully
   */
  
  feed_time.set_store_id(2);
  assert(feed_time.get_store_id() == 2);
  
  /*
   * test if a simple serializer and deserializer couple, allows loading and storing of a value
   */
  
  feed_time.set_serializer(store);
  feed_time.set_deserializer(retrieve);
  
  feed_time.load();
  assert(deserialization_count == 1);
  assert(feed_time.get() == 5);
  
  feed_time.set(20);
  assert(feed_time.get() == 20);
  
  feed_time.set(20);
  
  /*
   * check if setting the same value two times, fired the serializer only once
   */
  
  assert(serialization_count == 1);
  
  /*
   * test if the default_value is used when deserializer is missing
   */
  
  fan_speed.load();
  assert(fan_speed.get() == 0);
  
  valve_overriden.set_default_value(true);
  valve_overriden.load();
  
  assert(valve_overriden.get() == true);
  
  /*
   * check if min and max gets stored
   */
  
  feed_time.set_min_value(-10);
  feed_time.set_max_value(20);
  
  assert(feed_time.get_min_value() == -10);
  assert(feed_time.get_max_value() == 20);
  
  return 0;
}
