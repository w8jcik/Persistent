#include "Persistent.h"

Persistent::Persistent(void) {
  this->default_value = 0;
  this->store_id = -1;
}

int Persistent::set(int new_value) {
  if (new_value != this->value) {
    this->value = new_value;

    if (this->serializer) {
      this->serializer(*this, new_value);
    } else {
      if (this->logger) {
        this->logger("Persistent.set: pass a serializer procedure before setting the variable");
      }
    }
  }

  // TODO: isn't this check redundant?

  if (this->observer) {
    if (this->value != this->observed_value) {
      this->observer(this->value);
      this->observed_value = this->value;
    }
  }

  return 0;
}

// TODO: write a test for the observer

void Persistent::set_observer(void (*new_observer)(int value)) {
  this->observer = new_observer;

  if (this->observer) {
    this->observer(this->value);
    this->observed_value = this->value;
  }
}

int Persistent::load(void) {
  if (this->deserializer) {
    this->value = this->deserializer(*this);
  } else {
    if (this->logger) {
      this->logger("Persistent.load: pass a deserializer procedure before getting the variable, using default value");
    }
    this->load_default_value();
  }
  return 0;
}

const char* Persistent::get_name(void) {
  if (this->name) {
    return this->name;
  } else {
    if (this->logger) {
      this->logger("Persistent.get_name: was called on a variable without a name");
    }
    return "unnamed";
  }
}

// Plain getters and setters

int Persistent::get(void) {
  return this->value;
}

void Persistent::set_serializer(void (*new_serializer)(Persistent& self, int new_value)) {
  this->serializer = new_serializer;
}

void Persistent::set_deserializer(int (*new_deserializer)(Persistent& self)) {
  this->deserializer = new_deserializer;
}

void Persistent::set_logger(void (*new_logger)(const char* message)) {
  this->logger = new_logger;
}

void Persistent::set_min_value(int new_min_value) {
  this->min_value = new_min_value;
}

void Persistent::set_max_value(int new_max_value) {
  this->max_value = new_max_value;
}

int Persistent::get_min_value() {
  return this->min_value;
}

int Persistent::get_max_value() {
  return this->max_value;
}

void Persistent::set_store_id(int new_store_id) {
  this->store_id = new_store_id;
}

int Persistent::get_store_id(void) {
  return this->store_id;
}

void Persistent::set_name(const char* new_name) {
  this->name = new_name;
}

void Persistent::set_default_value(int new_default_value) {
  this->default_value = new_default_value;
}

void Persistent::load_default_value() {
  this->value = this->default_value;
}

// TODO: should be put into a subclass

int Persistent::get_changed_time(void) {
  return this->changed_time;
}

void Persistent::set_changed_time(int new_time) {
  this->changed_time = new_time;
}
