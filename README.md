# Persistent

An object that gets serialized, usually stored somewhere, when set. You are passing external procedures that handle serialization and deserialization.

## Example

```c++
Persistent feed_time;

int stored_value = 5;

void store(Persistent & self, int new_value) {
  //~ printf("saving as [%s] file\n", self.get_name());
  //~ printf("saving also at memory address [%d]\n", self.get_store_id());
  
  stored_value = new_value;
}

int retrieve(Persistent & self) {
  //~ printf("retrieving data from [%s] file\n", self.get_name());
  //~ printf("retrieving data from [%d] memory address \n", self.get_store_id());
  
  return stored_value;
}

void logger(const char* message) {
  printf("%s\n", message);
}

int main() {
  
  feed_time.set_logger(logger);
  
  feed_time.set_name("feed_time");
  feed_time.set_store_id(2);
  
  feed_time.set_default_value(30);
  
  feed_time.set_serializer(store);
  feed_time.set_deserializer(retrieve);
  
  feed_time.load();
  
  assert(feed_time.get() == 5);
  
  feed_time.set(20);
  assert(feed_time.get() == 20);
  
  return 0;
}
```

Inside the serializer/deserializer you can use public methods of the class.

Usually you need multiple Persistent objects. You can put them into an array and loop over them to assign serializers/deserializers.

```c++
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

Persistent feed_time, fan_speed, valve_overriden;

Persistent *settings[] = {&feed_time, &fan_speed, &valve_overriden};

void store(Persistent & self, int new_value) { ... };
int retrieve(Persistent & self) { ... };

int main() {

  for (i = 0; i < NELEMS(settings); i += 1) {
    (*settings[i]).set_serializer(store);
    (*settings[i]).set_deserializer(retrieve);
    (*settings[i]).set_store_id(i);
    
    (*settings[i]).load();
  }

  return 0;
}

```

## API

`int set(int new_value)`  
`int get()`

`void set_logger(void (*new_logger)(const char *message))`  
`void set_serializer(void (*new_serializer)(Persistent & self, int new_value))`  
`void set_deserializer(int (*new_deserializer)(Persistent & self))`  

`void set_default_value(int new_default_value)`
`int load()`

These are supposed to be used inside serializers and deserializers.

`void set_store_id(int new_store_id)`  
`int get_store_id()`

`void set_name(const char* new_name)`  
`const char* get_name()`

`void load_default_value()`

You can store minimum and maximum values, not used internally. You might want to pass them to a GUI or use inside the serializers/deserializers.

`void set_min_value(int new_min_value)`  
`void set_max_value(int new_max_value)`
 
`int get_min_value()`  
`int get_max_value()`  

## Tests

```
$ make tests
```
